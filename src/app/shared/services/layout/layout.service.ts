import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
// import { getHttpHeaderOptions } from '../header';

@Injectable({
  providedIn: 'root'
})

export class LayoutService {

    constructor(private httpClient: HttpClient) { }

    getVariableList(limit, skip, text) {
    	let params = new HttpParams();
    	params = params.append('limit', limit);
    	params = params.append('skip', skip);
    	if(text){
    		params = params.append('text', text);
    	}
        return this.httpClient.get(`${environment.apiURL}v1/socket/list`,{params: params});
    }

}
