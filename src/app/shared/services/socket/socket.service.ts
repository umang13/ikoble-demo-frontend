import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private namespace = '';
  private url = `${environment.apiURL}`;
  private socket;

  constructor() { }

    // connect socket
    public connect() {
        return Observable.create(observer => {

            this.socket  = io.connect(this.url);
            this.socket.on('connect', () => {
                if (this.socket.connected) {
                    console.log(`socket connected`);
                    observer.next(this.socket);
                } else {
                    console.log(`socket not connected`);
                }
            });

          this.socket.on('disconnect', () => {
              console.log(`socket disconnected`);
          });
      });
   }

   // join socket room
    public joinRoom(room) {
        return Observable.create(observer => {
            this.socket.emit('join', room);
            this.socket.on('joined', () => {
                observer.next('joined');
                observer.complete();
            });
        });
    }

    // leave socket room
    public leaveRoom(room) {
      console.log('leave room called', room);
      this.socket.emit('leave', room);
    }

    // listening socket data
    public listen(name) {
        return Observable.create(observer => {
            this.socket.on(name, message => {
              observer.next(message);
            });
        });
    }
}
