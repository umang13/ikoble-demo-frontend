import { Injectable } from '@angular/core';
import { SocketService } from './socket.service';

@Injectable({
  providedIn: 'root'
})
export class MessageSocketService extends SocketService {

  constructor() {
    super();
  }

  public connect() {
    return super.connect();
  }

  public receiveMessage() {
    return super.listen('strrec');
  }
}
