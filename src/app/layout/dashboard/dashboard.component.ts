import { Component, OnInit, ViewChild  } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { LayoutService } from 'src/app/shared/services/layout/layout.service';
import { MessageSocketService } from 'src/app/shared/services/socket/message-socket.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

export interface PeriodicElement {
	Id: string;
	Label: string;
	String: string;
	Timestamp: any;
	_id:string;
	type:string;
	unique_str:string;
	timestamp:any;
}

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

	public listing: any = [];
	public loading = false;
	public serverError: any = null;
	public totalPage: any = 100;
	public searchString:string = '';
	public currentPageSize:any = 10;

	@ViewChild('table2', { read: MatSort}) sort: MatSort;
	@ViewChild('table1', { read: MatPaginator, static: true }) paginator1: MatPaginator;
	@ViewChild('table2', { read: MatPaginator, static: true }) paginator2: MatPaginator;

	displayedColumns1: string[] = ['Id', 'Label', 'String', 'Timestamp'];
	displayedColumns2: string[] = ['_id', 'type', 'unique_str', 'timestamp'];

	socketDataSource 	= new MatTableDataSource();
	listDataSource 		= new MatTableDataSource();
	//@ViewChild(MatSort)

	constructor(private layoutService: LayoutService, private messagingSocketService:MessageSocketService, private snackBar: MatSnackBar) {
		this.messagingSocketService.connect().subscribe(() => {
	      	console.log('messaging socket connected.');
	    });
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.searchString = filterValue;
		this.bind(this.currentPageSize, 0, filterValue);
		// this.listDataSource.filter = filterValue.trim().toLowerCase();
	}

	ngOnInit() {

		this.bind(10, 0);
		//this.listDataSource.sort = this.sort;
		// join socket on page initialization
		/*this.messagingSocketService.joinRoom().subscribe(() => {
	      console.log('messaging socket join.');
	    });*/

		// rechive socket data
		this.messagingSocketService.receiveMessage().subscribe(message => {
	      	this.socketDataSource.data  = [message, ...this.socketDataSource.data];
	      	this.socketDataSource 		= new MatTableDataSource(this.socketDataSource.data);
	      	this.socketDataSource.paginator = this.paginator1;
	    });
	}

	changePage(event){
		let page = (event.pageIndex > 0)? event.pageIndex-1 : 0;
		this.currentPageSize = event.pageSize;
		this.bind(event.pageSize, page, this.searchString);
	}

	bind(limit, skip, text=null) 
	{
		this.layoutService.getVariableList(limit, skip, text).subscribe(
			(response: any) => {
				//Success message
				this.listing   			 = response.record_details;
				this.listDataSource 	 = new MatTableDataSource(this.listing);
				this.listDataSource.sort = this.sort;
				this.totalPage 			 = response.all_record;
			},
			(err: any) => {
				//handle error
				this.serverError = err.error.message;
			}
		);
	}

}
